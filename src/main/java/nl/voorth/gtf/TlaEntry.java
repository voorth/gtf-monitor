/*
 * Created on Apr 2, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package nl.voorth.gtf;

/**
 * @author voorth
 * <p>
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class TlaEntry
{
  private String tla;
  private String description;
  private String canonical;

  TlaEntry(String line)
  {
    String[] terms = line.split("\\t");
    init(terms[0], terms[1]);
  }

  void init(String t, String desc)
  {
    tla = t;
    description = desc.trim();
    canonical = description.replaceAll(" *\\[[^]]+\\] *", " ")
      .trim();
  }

  /**
   * @return Returns the tla.
   */
  public String getTla()
  {
    return tla;
  }

  /**
   * @return Returns the canonical.
   */
  public String getCanonical()
  {
    return canonical;
  }

  /**
   * @return Returns the description.
   */
  public String getDescription()
  {
    return description;
  }

  public String toString()
  {
    return tla + "\t" + description;
  }

}
