package nl.voorth.gtf.ui;

import java.awt.Color;
import java.awt.Component;
import java.util.Enumeration;

import javax.swing.JTree;
import javax.swing.tree.*;


public class TlaTreeCellRenderer extends DefaultTreeCellRenderer
{

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  private static final Color NEW_ENTRY_COLOR = Color.GREEN.darker();

  @Override
  public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus)
  {
    DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer) super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
    if (value instanceof DefaultMutableTreeNode)
    {
      DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
      Object obj = node.getUserObject();
      if (obj instanceof TlaEntry)
      {
        if (((TlaEntry) obj).isNewEntry()) renderer.setForeground(NEW_ENTRY_COLOR);
      }
      else
      {
        String text = renderer.getText();
        renderer.setText(text + " [" + countEntries(node) + "/" + countTlas(node) + "]");
      }
    }
    return renderer;
  }

  private int countEntries(DefaultMutableTreeNode node)
  {
    if (node.getUserObject() instanceof TlaEntry) return 1;
    int children = 0;
    for (Enumeration<?> e = node.children(); e.hasMoreElements(); )
    {
      DefaultMutableTreeNode subNode = (DefaultMutableTreeNode) e.nextElement();
      children += countEntries(subNode);
    }
    return children;
  }

  private int countTlas(DefaultMutableTreeNode node)
  {
    Object userObject = node.getUserObject();
    if (userObject instanceof String && ((String) userObject).length() == 3) return 1;
    int children = 0;
    for (Enumeration<?> e = node.children(); e.hasMoreElements(); )
    {
      DefaultMutableTreeNode subNode = (DefaultMutableTreeNode) e.nextElement();
      children += countTlas(subNode);
    }
    return children;
  }

}
