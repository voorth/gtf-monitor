package nl.voorth.gtf.ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Map.Entry;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;

import nl.voorth.gtf.TlaFilter;


public class TlaTreeModel extends DefaultTreeModel
{

  /**
   *
   */
  private static final long serialVersionUID = -952729911611953023L;
  private List<TlaFilter> filters = new LinkedList<TlaFilter>();
  private Map<String, SortedSet<TlaEntry>> tlaMap = new LinkedHashMap<String, SortedSet<TlaEntry>>();  //  @jve:decl-index=0:

  private Map<String, SortedSet<TlaEntry>> filteredMap = new LinkedHashMap<String, SortedSet<TlaEntry>>(tlaMap);

  public TlaTreeModel()
  {
    super(new DefaultMutableTreeNode());
  }

  public TlaTreeModel(TreeNode root)
  {
    super(root);
  }

  private void addChild(DefaultMutableTreeNode node, String key, String rest, Set<TlaEntry> entries)
  {
    if (rest.length() == 0)
    {
      entries
        .stream()
        .forEach(v -> node.insert(new DefaultMutableTreeNode(v), node.getChildCount()));
      return;
    }

    key += rest.substring(0, 1);
    rest = rest.substring(1);
    DefaultMutableTreeNode childNode = null;
    for (Enumeration<TreeNode> enm = node.children(); enm.hasMoreElements(); )
    {
      TreeNode tmpNode = enm.nextElement();
      if (tmpNode.toString()
        .equals(key))
      {
        childNode = (DefaultMutableTreeNode) tmpNode;
        break;
      }
    }
    if (childNode == null)
    {
      childNode = new DefaultMutableTreeNode(key);
      node.insert(childNode, node.getChildCount());
    }
    addChild(childNode, key, rest, entries);

  }

  public void setFilter(TlaFilter filter, boolean add)
  {
    if (add) filters.add(filter);
    else filters.remove(filter);
    reset();
  }

  /**
   *
   */
  public void reset()
  {
    DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode();
    setRoot(rootNode);
    setMap(rootNode, buildFilteredMap());
    fireTreeStructureChanged(rootNode, new Object[]{rootNode}, null, null);
  }

  private Map<String, SortedSet<TlaEntry>> buildFilteredMap()
  {
    filteredMap = tlaMap;
    for (TlaFilter filter : filters)
    {
      filteredMap = getMap(filteredMap, filter);
    }
    return filteredMap;
  }

  private Map<String, SortedSet<TlaEntry>> getMap(Map<String, SortedSet<TlaEntry>> filteredMap, TlaFilter filter)
  {
    final HashMap<String, SortedSet<TlaEntry>> map = new LinkedHashMap<String, SortedSet<TlaEntry>>();
//    filteredMap
//      .entrySet()
//      .stream()
//      .filter(filter::acceptTla)
//      .filter
    for (Entry<String, SortedSet<TlaEntry>> entry : filteredMap.entrySet())
    {
      if (filter.acceptTla(entry))
      {
        final SortedSet<TlaEntry> values = new TreeSet<TlaEntry>();
        for (TlaEntry value : entry.getValue())
        {
          if (filter.acceptValue(entry.getKey(), value))
            values.add(value);
        }
        if (filter.acceptValues(values))
        {
          map.put(entry.getKey(), values);
        }
      }
    }
    return map;
  }

  public Map<String, ? extends SortedSet<TlaEntry>> getMap(TlaFilter filter)
  {
    Map<String, SortedSet<TlaEntry>> srcMap = tlaMap;
    return getMap(srcMap, filter);
  }

  ;

  private void initTlaMap()
  {
    for (char i = 'A'; i <= 'Z'; i++)
    {
      for (char j = 'A'; j <= 'Z'; j++)
      {
        for (char k = 'A'; k <= 'Z'; k++)
        {
          String tla = new String(new char[]{i, j, k});
          tlaMap.put(tla, new TreeSet<TlaEntry>());
        }
      }
    }
  }

  private void readGtf(final String urlString) throws PrivilegedActionException
  {
    AccessController.doPrivileged(new PrivilegedExceptionAction<Object>()
    {
      public Object run() throws PrivilegedActionException
      {
        try
        {
          BufferedReader in = new BufferedReader(new InputStreamReader(new URL(urlString).openStream()));
          readTlaList(in, false);
        }
        catch (IOException e)
        {
          throw new PrivilegedActionException(e);
        }
        return null;
      }
    });

  }

  public void addNewEntry(String tla, String description)
  {
    addEntry(tla, description, true);
  }

  private void addEntry(String tla, String description, boolean isNewEntry)
  {
    tlaMap.get(tla)
      .add(new TlaEntry(description, isNewEntry));
  }

  private void setMap(DefaultMutableTreeNode root, Map<String, SortedSet<TlaEntry>> name)
  {
    for (Entry<String, ? extends Set<TlaEntry>> entry : name.entrySet())
    {
      addChild(root, "", entry.getKey(), entry.getValue());
    }
  }

  private void setMap(Map<String, SortedSet<TlaEntry>> tlaMap)
  {
    setMap((DefaultMutableTreeNode) getRoot(), tlaMap);
  }

  public void setUrl(String url) throws PrivilegedActionException
  {
    initTlaMap();
    readGtf(url);
    setMap(tlaMap);
  }

  public Map<String, SortedSet<TlaEntry>> getFilteredMap()
  {
    return filteredMap;
  }

  private void readTlaList(BufferedReader reader, boolean isNew) throws IOException
  {

    try (reader)
    {
      reader.lines()
        .map(line -> line.split("\\t"))
        .filter(arr -> arr.length >= 2)
        .forEach(words -> tlaMap.get(words[0])
          .add(new TlaEntry(words[1], isNew)));
    }
  }

}
