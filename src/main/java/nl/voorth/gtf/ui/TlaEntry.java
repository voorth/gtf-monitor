/**
 *
 */
package nl.voorth.gtf.ui;

public class TlaEntry implements Comparable<TlaEntry>
{
  public final String description;
  public boolean newEntry;

  TlaEntry(String description, boolean newEntry)
  {
    this.description = description;
    this.newEntry = newEntry;
  }

  public TlaEntry(String description)
  {
    this(description, false);
  }

  public int compareTo(TlaEntry that)
  {
    return this.description.compareTo(that.description);
  }

  public String toString()
  {
    return description;
  }

  public boolean isNewEntry()
  {
    return newEntry;
  }

  public void setNewEntry(boolean newEntry)
  {
    this.newEntry = newEntry;
  }
}