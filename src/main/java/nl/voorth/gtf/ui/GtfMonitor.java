package nl.voorth.gtf.ui;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.MalformedURLException;


public class GtfMonitor extends Panel
{

  /**
   *
   */
  private static final long serialVersionUID = -8943580354027753611L;

  public static void main(String[] args)
  {
    Frame frame = new Frame();
    frame.addWindowListener(new java.awt.event.WindowAdapter()
    {
      public void windowClosing(java.awt.event.WindowEvent e)
      {
        System.exit(0);
      }

      ;
    });

    GtfMonitor gtfMonitor = new GtfMonitor();
    gtfMonitor.setSize(800, 600); // same size as defined in the HTML APPLET
    gtfMonitor.setPreferredSize(new Dimension(800, 600));
    frame.add(gtfMonitor);
    gtfMonitor.init();
    frame.pack();
    frame.setSize(800, 600 + 20); // add 20, seems enough for the Frame title,
    frame.setVisible(true);
  }

  /**
   * This method initializes this
   *
   * @throws IOException
   * @throws MalformedURLException
   */
  public void init()
  {
    try
    {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch (ClassNotFoundException |
      InstantiationException |
      IllegalAccessException |
      UnsupportedLookAndFeelException e1)
    {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    this.add(GtfContent.getJContentPane());
  }


  protected String paramString()
  {
    return super.paramString();
  }


}
