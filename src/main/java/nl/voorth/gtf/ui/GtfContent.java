package nl.voorth.gtf.ui;

import nl.voorth.gtf.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.MalformedURLException;
import java.security.PrivilegedActionException;
import java.util.Map;
import java.util.SortedSet;

public class GtfContent
{
  private static JPanel jContentPane = null;
  private static JScrollPane scrEntries = null;
  private static JTree trEntries = null;
  private static JToolBar jToolBar = null;
  private static JLabel lblUrl = null;
  private static JTextField fldUrl = null;
  private static JPanel pnlTools = null;
  private static JButton btnMail = null;
  private static JPanel pnlFilters = null;
  private static JCheckBox chkEmptyFilter = null;
  private final static TlaTreeModel tlaTreeModel = new TlaTreeModel();
  private static JCheckBox chkRegex = null;
  private static JCheckBox chkExclude = null;
  private static JPanel pnlFiller = null;
  private static JTextField fldRegex = null;
  private static JLabel lblStats = null;
  private static JLabel lblError = null;
  private static JPanel pnlEntries = null;
  private static JButton btnAddEntry = null;

  private static TlaTreeCellRenderer tlaTreeCellRenderer = null;

  /**
   * This method initializes jContentPane
   *
   * @return javax.swing.JPanel
   */

  public static JPanel getJContentPane()
  {
    if (jContentPane == null)
    {
      BorderLayout borderLayout = new BorderLayout();
      borderLayout.setHgap(5);
      borderLayout.setVgap(5);
      jContentPane = new JPanel();
      jContentPane.setLayout(borderLayout);
      jContentPane.add(getJToolBar(), BorderLayout.NORTH);
      jContentPane.add(getPnlTools(), BorderLayout.SOUTH);
      jContentPane.add(getPnlFilters(), BorderLayout.EAST);
      jContentPane.add(getPnlEntries(), BorderLayout.CENTER);
    }
    return jContentPane;
  }

  /**
   * This method initializes scrEntries
   *
   * @return javax.swing.JScrollPane
   * @throws IOException
   * @throws MalformedURLException
   */
  private static JScrollPane getScrEntries()
  {
    if (scrEntries == null)
    {
      scrEntries = new JScrollPane();
      scrEntries.setViewportView(getTrEntries()); // Generated
    }
    return scrEntries;
  }

  /**
   * This method initializes trEntries
   *
   * @return javax.swing.JTree
   * @throws IOException
   * @throws MalformedURLException
   */
  private static JTree getTrEntries()
  {
    if (trEntries == null)
    {
      try
      {
        tlaTreeModel.setUrl("https://jtv.home.xs4all.nl/gtf/GPL_TLA_FAQ");
      }
      catch (PrivilegedActionException e)
      {
        getFldUrl().setBackground(Color.PINK);
        getFldUrl().setText(
          e.getClass()
            .getSimpleName() + ": " + e.getMessage());
        e.printStackTrace();
      }
      tlaTreeCellRenderer = new TlaTreeCellRenderer();
      trEntries = new JTree();
      trEntries.setRootVisible(false);
      trEntries.setCellRenderer(tlaTreeCellRenderer);
      trEntries.setModel(tlaTreeModel); // Generated
    }
    return trEntries;
  }

  /**
   * This method initializes jToolBar
   *
   * @return javax.swing.JToolBar
   */
  private static JToolBar getJToolBar()
  {
    if (jToolBar == null)
    {
      lblUrl = new JLabel();
      lblUrl.setText("Address"); // Generated
      jToolBar = new JToolBar();
      jToolBar.setFloatable(false); // Generated
      jToolBar.add(lblUrl); // Generated
      jToolBar.add(getFldUrl()); // Generated
    }
    return jToolBar;
  }

  /**
   * This method initializes fldUrl
   *
   * @return javax.swing.JTextField
   */
  private static JTextField getFldUrl()
  {
    if (fldUrl == null)
    {
      fldUrl = new JTextField();
      fldUrl.setText("http://www.xs4all.nl/~jtv/gtf/GPL_TLA_FAQ"); // Generated
      fldUrl.setEditable(false);
    }
    return fldUrl;
  }

  /**
   * This method initializes pnlTools
   *
   * @return javax.swing.JPanel
   */
  private static JPanel getPnlTools()
  {
    if (pnlTools == null)
    {
      lblStats = new JLabel();
      lblStats.setText(getStats());

      pnlTools = new JPanel();
      pnlTools.setLayout(new GridBagLayout()); // Generated
      pnlTools.add(getBtnMail(), btnMailConstraints()); // Generated
      pnlTools.add(lblStats, lbStatsConstraints()); // Generated
      pnlTools.add(getLblError(), lblErrorConstraints()); // Generated
    }
    return pnlTools;
  }

  private static JLabel getLblError()
  {
    lblError = new JLabel();
    lblError.setText(""); // Generated
    lblError.setForeground(new Color(192, 0, 0));
    return lblError;
  }

  private static GridBagConstraints lbStatsConstraints()
  {
    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(5, 5, 5, 5);
    constraints.gridy = 0;
    constraints.anchor = GridBagConstraints.SOUTHWEST; // Generated
    constraints.gridx = 0;
    return constraints;
  }

  private static GridBagConstraints btnMailConstraints()
  {
    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(5, 5, 5, 5);
    constraints.gridy = 0;
    constraints.anchor = GridBagConstraints.SOUTHEAST; // Generated
    constraints.weightx = 0.0; // Generated
    constraints.gridx = 2;
    return constraints;
  }

  private static GridBagConstraints lblErrorConstraints()
  {
    GridBagConstraints constraints = new GridBagConstraints();
    constraints.gridx = 1; // Generated
    constraints.anchor = GridBagConstraints.SOUTH; // Generated
    constraints.fill = GridBagConstraints.HORIZONTAL; // Generated
    constraints.weightx = 1.0; // Generated
    constraints.insets = new Insets(5, 5, 5, 5); // Generated
    constraints.gridy = 0; // Generated
    return constraints;
  }

  /**
   * This method initializes btnMail
   *
   * @return javax.swing.JButton
   */
  private static JButton getBtnMail()
  {
    if (btnMail == null)
    {
      btnMail = new JButton();
      btnMail.setText("Send TLA's"); // Generated
      btnMail.setHorizontalAlignment(SwingConstants.RIGHT); // Generated
    }
    return btnMail;
  }

  /**
   * This method initializes pnlFilters
   *
   * @return javax.swing.JPanel
   */
  private static JPanel getPnlFilters()
  {
    if (pnlFilters == null)
    {
      GridBagConstraints gbChkEmptyFilter = new GridBagConstraints();
      gbChkEmptyFilter.insets = new Insets(5, 5, 5, 5);
      gbChkEmptyFilter.gridy = 0;
      gbChkEmptyFilter.gridx = 0;
      gbChkEmptyFilter.gridwidth = 2;
      gbChkEmptyFilter.anchor = GridBagConstraints.WEST;

      GridBagConstraints gbChkRegex = new GridBagConstraints();
      gbChkRegex.insets = new Insets(5, 5, 5, 5);
      gbChkRegex.gridy = 1;
      gbChkRegex.gridx = 0;

      GridBagConstraints gbFldRegex = new GridBagConstraints();
      gbFldRegex.insets = new Insets(5, 5, 5, 5);
      gbFldRegex.gridx = 1;
      gbFldRegex.gridy = 1;
      gbFldRegex.weightx = 1.0;
      gbFldRegex.fill = GridBagConstraints.VERTICAL;

      GridBagConstraints gbChkExclude = new GridBagConstraints();
      gbChkExclude.insets = new Insets(5, 5, 5, 5);
      gbChkExclude.gridx = 0;
      gbChkExclude.gridy = 2;
      gbChkExclude.gridwidth = 2;
      gbChkExclude.anchor = GridBagConstraints.WEST;

      GridBagConstraints gbFiller = new GridBagConstraints();
      gbFiller.insets = new Insets(5, 5, 5, 5);
      gbFiller.gridy = 3;
      gbFiller.gridwidth = 2;
      gbFiller.fill = GridBagConstraints.BOTH;
      gbFiller.weighty = 1.0D;
      gbFiller.gridx = 0;

      pnlFilters = new JPanel();
      pnlFilters.setLayout(new GridBagLayout());
      pnlFilters.add(getChkEmptyFilter(), gbChkEmptyFilter);
      pnlFilters.add(getChkRegex(), gbChkRegex);
      pnlFilters.add(getFldRegex(), gbFldRegex);
      pnlFilters.add(getChkExclude(), gbChkExclude);
      pnlFilters.add(getPnlFiller(), gbFiller);
    }
    return pnlFilters;
  }

  /**
   * This method initializes chkEmptyFilter
   *
   * @return javax.swing.JCheckBox
   */
  private static JCheckBox getChkEmptyFilter()
  {
    if (chkEmptyFilter == null)
    {
      chkEmptyFilter = new JCheckBox();
      chkEmptyFilter.setText("Show empty entries");
      chkEmptyFilter.addActionListener(GtfContent::doCheckEmpty);
    }
    return chkEmptyFilter;
  }

  private static void doCheckEmpty(ActionEvent e)
  {
    checkFilter(EmptyTlaFilter.instance(), chkEmptyFilter);
    updateStats();
  }

  /**
   * This method initializes chkRegex
   *
   * @return javax.swing.JCheckBox
   */
  private static JCheckBox getChkRegex()
  {
    if (chkRegex == null)
    {
      chkRegex = new JCheckBox();
      chkRegex.setText("Match:");
      chkRegex.addActionListener(GtfContent::doCheckRegex);
    }
    return chkRegex;
  }

  private static void doCheckRegex(ActionEvent e)
  {
    boolean hasRegex = chkRegex.isSelected();
    getFldRegex().setEnabled(hasRegex);
    tlaTreeModel.setFilter(RegexFilter.instance(), hasRegex);
    updateStats();
  }

  /**
   * This method initializes chkExclude
   *
   * @return javax.swing.JCheckBox
   */
  private static JCheckBox getChkExclude()
  {
    if (chkExclude == null)
    {
      chkExclude = new JCheckBox();
      chkExclude.setText("Exclude known misses");
      chkExclude.addActionListener(GtfContent::doCheckExclude);
    }
    return chkExclude;
  }

  private static void doCheckExclude(ActionEvent e)
  {
    checkFilter(ExcludeListFilter.instance(), chkExclude);
  }

  private static void checkFilter(TlaFilter filter, JCheckBox checkBox)
  {
    tlaTreeModel.setFilter(filter, checkBox.isSelected());
    updateStats();
  }

  /**
   * This method initializes pnlFiller
   *
   * @return javax.swing.JPanel
   */
  private static JPanel getPnlFiller()
  {
    if (pnlFiller == null)
    {
      pnlFiller = new JPanel();
      pnlFiller.setLayout(new GridBagLayout());
    }
    return pnlFiller;
  }

  /**
   * This method initializes fldRegex
   *
   * @return javax.swing.JTextField
   */
  private static JTextField getFldRegex()
  {
    if (fldRegex == null)
    {
      fldRegex = new JTextField();
      fldRegex.setPreferredSize(new Dimension(160, 22));
      fldRegex.setEnabled(false);
      fldRegex.addActionListener(GtfContent::doRegexChanged);
    }
    return fldRegex;
  }

  private static void doRegexChanged(ActionEvent e)
  {
    RegexFilter.instance()
      .setRegex(fldRegex.getText());
    resetModel();
  }

  /**
   *
   */
  private static void resetModel()
  {
    tlaTreeModel.reset();
    updateStats();
  }

  /**
   *
   */
  private static void updateStats()
  {
    final String stats = getStats();
    lblStats.setText(stats);
  }

  /**
   * @return
   */
  private static String getStats()
  {
    final Map<String, SortedSet<TlaEntry>> tlaMap = tlaTreeModel
      .getFilteredMap();
    int tlaCount = 0;
    int valueCount = 0;
    for (SortedSet<TlaEntry> values : tlaMap.values())
    {
      if (values.size() > 0)
      {
        tlaCount++;
        valueCount += values.size();
      }
    }
    float pct = (100f * tlaCount) / (26 * 26 * 26);

    final String stats = tlaCount + "/" + valueCount + " (" + pct + "%)";
    return stats;
  }

  /**
   * This method initializes pnlEntries
   *
   * @return javax.swing.JPanel
   */
  private static JPanel getPnlEntries()
  {
    if (pnlEntries == null)
    {
      GridBagConstraints addEntryConstraints = new GridBagConstraints();
      addEntryConstraints.gridx = 0;
      addEntryConstraints.anchor = GridBagConstraints.WEST;
      addEntryConstraints.gridy = 1;
      GridBagConstraints entriesConstraints = new GridBagConstraints();
      entriesConstraints.fill = GridBagConstraints.BOTH;
      entriesConstraints.weighty = 1.0;
      entriesConstraints.weightx = 1.0;
      pnlEntries = new JPanel();
      pnlEntries.setLayout(new GridBagLayout());
      pnlEntries.setPreferredSize(new Dimension(400, 384));
      pnlEntries.add(getScrEntries(), entriesConstraints);
      pnlEntries.add(getBtnAddEntry(), addEntryConstraints);
    }
    return pnlEntries;
  }

  /**
   * This method initializes btnAddEntry
   *
   * @return javax.swing.JButton
   */
  private static JButton getBtnAddEntry()
  {
    if (btnAddEntry == null)
    {
      btnAddEntry = new JButton();
      btnAddEntry.setText("+");
      btnAddEntry.setMaximumSize(new Dimension(40, 29));
      btnAddEntry.setMinimumSize(new Dimension(40, 29));
      btnAddEntry.setToolTipText("Add TLA");
      btnAddEntry.setPreferredSize(new Dimension(40, 29));
    }
    return btnAddEntry;
  }
}
