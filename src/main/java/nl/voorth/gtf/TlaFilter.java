package nl.voorth.gtf;

import java.util.SortedSet;
import java.util.Map.Entry;

import nl.voorth.gtf.ui.TlaEntry;

public interface TlaFilter
{

  public default boolean acceptTla(Entry<String, SortedSet<TlaEntry>> entry)
  {
    return true;
  }

  public default boolean acceptValue(String tla, TlaEntry value)
  {
    return true;
  }

  public default boolean acceptValues(SortedSet<TlaEntry> values)
  {
    return true;
  }

}