/**
 *
 */
package nl.voorth.gtf;

import java.util.SortedSet;
import java.util.Map.Entry;

import nl.voorth.gtf.ui.TlaEntry;

public class EmptyTlaFilter implements TlaFilter
{
  private static final EmptyTlaFilter INSTANCE = new EmptyTlaFilter();

  /* (non-Javadoc)
   * @see voorth.lab.gtf.TlaFilter#accept(java.util.Map.Entry)
   */
  @Override
  public boolean acceptTla(Entry<String, SortedSet<TlaEntry>> entry)
  {
    return entry.getValue()
      .size() == 0;
  }

  public static EmptyTlaFilter instance()
  {
    return INSTANCE;
  }
}