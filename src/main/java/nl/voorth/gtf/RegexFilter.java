package nl.voorth.gtf;

import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.LITERAL;

import java.util.SortedSet;
import java.util.regex.Pattern;

import nl.voorth.gtf.ui.TlaEntry;


public class RegexFilter implements TlaFilter
{
  private static final RegexFilter INSTANCE = new RegexFilter();

  private String regex;
  private Pattern pattern;


  @Override
  public boolean acceptValues(SortedSet<TlaEntry> values)
  {
    return values.size() > 0;
  }

  @Override
  public boolean acceptValue(String tla, TlaEntry value)
  {
    if (regex == null || regex.length() == 0) return true;
    return pattern.matcher(value.description)
      .find();
  }

  public void setRegex(String reg)
  {
    regex = reg;
    pattern = Pattern.compile(reg, CASE_INSENSITIVE + LITERAL);
  }

  public static RegexFilter instance()
  {
    return INSTANCE;
  }
}
