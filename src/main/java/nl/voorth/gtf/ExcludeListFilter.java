package nl.voorth.gtf;

import java.util.*;
import java.util.Map.Entry;

import nl.voorth.gtf.ui.TlaEntry;

public class ExcludeListFilter implements TlaFilter
{
  private static TlaFilter INSTANCE;

  SortedSet<String> exclude = new TreeSet<String>();

  public ExcludeListFilter()
  {
    exclude.add("AJX");
  }

  @Override
  public boolean acceptTla(Entry<String, SortedSet<TlaEntry>> entry)
  {
    return (!exclude.contains(entry.getKey()));
  }

  public static TlaFilter instance()
  {
    if (INSTANCE == null)
    {
      INSTANCE = new ExcludeListFilter();
    }
    return INSTANCE;
  }

}
